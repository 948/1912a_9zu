import http from '@/utils/httpTool';
//获取知识小测数据
export const knListApi = (params: any) => http.get('/api/knowledge', params);
//搜索
export const knSearchApi = (params: any) => http.get('/api/knowledge', params);
// 改变发布草稿状态
export const knStateApi = ({ id, status }: any) =>
  http.patch('/api/knowledge/' + id, { status: status === 'draft' ? 'publish' : 'draft' });
//新建
export const knAddApi = (parmas: {}) => http.post('/api/knowledge/book', parmas);
//删除
export const delKnApi=(id:string) =>http.delete('/api/knowledge/'+id) 
// 搜索评论
export const findcommitApi=(params:{})=>http.get('/api/comment',params)
