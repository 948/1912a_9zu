import http from '@/utils/httpTool';

// 获取登录接口 (api传入相对的信息)
export const login = data => http.post('/api/auth/login', data);
//注册
export const register = data => http.post('/api/user/register', data);
// 用户
export const getUserList = params => http.get('/api/user', params);

// 添加标签
export const addTagList = data => http.post('/api/tag', data);

// 标签页 数据
export const getTagList = () => http.get('/api/tag');

// 搜索记录数据
export const getSearch = () => http.get('/api/search', { page: 1, pageSize: 12 });

// 评论管理数据
export const getComment = () => http.get('/api/comment', { page: 1, pageSize: 12 });

export const getList = () => http.get('/api/article');
// 搜索数据
export const searchList = params => http.get('/api/article', params);

// 知识小册数据
export const getKnowledge = () => http.get('/api/knowledge', { page: 1, pageSize: 12 });

//访问统计数据
export const getView = () => http.get('/api/view', { page: 1, pageSize: 12 });

//分类管理
export const getCategory = () => http.get('/api/category');




// 删除article
export const dellist = (id: string) =>http.delete(`/api/article/${id}`)
// 推荐
export const tuijian = (id: string) =>http.patch(`/api/article/${id}`)


export const registerApi = (data: datatype) => http.post('/api/user/register', data);
export const getarticle = (params?: any) => http.get('/api/article', params);



//删除功能
export const delFileApi = (id: string) => http.delete('/api/file/' + id);

export const getPagemanagement = (params: { page: number; pageSize: number }) => http.get('/api/page', params);
//页面管理搜索数据
export const getPagesearch = (params: { page: number; pageSize: number }) => http.get('/api/page', params);

export const getcomment = (params?: any) => http.get('/api/comment', params);

//用户管理数据
export const getPage=()=>http.get("/api/page",{page:1,pageSize:12})
// 文件
export const getFile = (parmas: any) => http.get('/api/file', parmas);
//获取文件管理数据
export const getFileApi = (parmas: any) => http.get('/api/file', parmas);

//搜索功能
export const serachFileApi = (params: any) => http.get('/api/file', params);

