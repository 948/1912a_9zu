import { defineStore } from 'pinia';
import * as user from '@/services/user';
import {
    registerApi,
    getFileApi,
    getarticle,
    serachFileApi,
    delFileApi,
    getPagesearch,
    getcomment,
    getPagemanagement,
} from '@/services/user';
//知识小测
import { knListApi, knSearchApi, knStateApi, knAddApi, delKnApi, findcommitApi } from '@/services/knowledge';
import { message } from 'ant-design-vue';
// "userStore"
export const useStore = defineStore('base', {
    state: () => {
        return {
            // 文章数据
            articleList: [],
            knList: [[], 0],
            itemObj: {},
            // 评论数据
            commentlist: [],
            //页面管理数据
            Pagelist: [],
            //评论搜索
            conList: [],
            searchlist: [], //搜索记录

            knowledgelist: [], //知识小册
            viewlist: [],  //访问统计
            pagelist: [], // 用户管理
            fileList: [[], 0],
        };
    },
    actions: {
        //注册
        async register(payload: { name: string; password: string }) {
            const data = await registerApi(payload);
            return data;
        },
        //文件管理
        async getFile(payload: any) {
            const { data } = await getFileApi(payload);

            this.$patch({
                fileList: [data[0], data[1]],
            });
        },
        //文件管理抽屉获取信息
        async getItem(payload: string) {
            console.log(this.$state.fileList[0], 'ksjdhfkshdfkj');
            console.log(payload, 'payload');

            this.$patch({
                //@ts-ignore
                itemObj: this.$state.fileList[0].find((item: any) => item.id === payload),
            });
        },
        //知识小测
        async getKnList(payload: any) {
            const { data } = await knListApi(payload);
            this.$patch({
                knList: [data[0], data[1]],
            });
        },
        //搜索
        async knSearch(payload: any) {
            console.log(payload);

            const { data } = await knSearchApi(payload);
            this.$patch({
                knList: [data[0], data[1]],
            });
        },
        //改变状态
        async knState(payload: any) {
            const { id, status } = payload;

            const data = await knStateApi(payload);
        },
        //新建
        async knAddList(payload: {}) {
            const data = await knAddApi(payload);
            if (data.statusCode === 201) {
                message.success('新增成功');
            } else {
                message.error('新增失败');
            }
        },
        //删除
        async deleteKn(id: string) {
            const data = await delKnApi(id);
            if (data.statusCode === 200) {
                message.success('删除成功');
                this.getKnList({ page: 1, pageSize: 12 });
            } else {
                message.error('删除失败');
            }
        },

        async getarticlelist(payload: { page: number; pageSize: number }) {
            const { data }: any = await getarticle(payload);
            this.$patch({
                articleList: data[0],
            });
        },
        // 最新评论数据
        async getcommentlist(payload: { page: number; pageSize: number }) {
            const { data }: any = await getcomment(payload);
            this.$patch({
                commentlist: data[0],
            });
        },
        // 点击通过
        async getpass(payload: any) {
            this.$state.commentlist.forEach((item: any) => {
                if (item.id == payload.id) {
                    item.pass = true;
                } else {
                    return;
                }
            });
        },
        // 点击未通过
        async getnopass(payload: any) {
            this.$state.commentlist.forEach((item: any) => {
                if (item.id == payload.id) {
                    item.pass = false;
                } else {
                    return;
                }
            });
        },
        //  删除评论
        async delwork(payload: any) {
            this.$patch({
                commentlist: this.$state.commentlist.filter((item: any) => item.id !== payload.id),
            });
        },

        // 搜索评论
        async findcommitlist(payload: any) {
            const { data } = await findcommitApi(payload)
            console.log(data[0], 'aaaaaaaaaa');

            this.$patch({
                commentlist: data[0]
            })
        },
        //页面管理数据
        async management(payload: { page: number; pageSize: number }) {
            const { data }: any = await getPagemanagement(payload);
            this.$patch({
                Pagelist: data[0],
            });
        },

        //页面管理搜索数据
        async serachpage(payload: any) {
            const { data } = await getPagesearch(payload);

            this.$patch({
                // fileList: [data[0], data[1]]
                Pagelist: data[0],
            });
        },
        // 搜索记录
        async getSearchAction() {
            const { data } = await user.getSearch();
            this.$patch({
                searchlist: data[0]
            })
            return data[0]
        },
        // 评论管理
        async getCommentAction() {
            const { data } = await user.getComment();
            console.log(data);
            this.$patch({
                commentlist: data[0]
            })
            return data[0]
        },
        // 知识小册
        async getKnowledgeAction() {
            const { data } = await user.getKnowledge();
            console.log("data555555555", data)
            this.$patch({
                knowledgelist: data[0]
            })
            return data[0]
        },
        // 访问统计
        async getViewAction() {
            const { data } = await user.getView();
            console.log("data222222", data);
            this.$patch({
                viewlist: data[0]
            })
            return data[0]
        },
        // 用户管理
        async getPageAction() {
            const { data } = await user.getPage();
            // console.log("data222222",data);
            this.$patch({
                pagelist: data[0]
            })
            return data[0]
        },
        //文件管理
        async getFileAction(payload: any) {
            const { data } = await user.getFile(payload);

            this.$patch({
                fileList: [data[0], data[1]],
            });
        },
        
        //文件搜索
        async serachFile(payload: any) {
            const { data } = await serachFileApi(payload);

            this.$patch({
                fileList: [data[0], data[1]],
            });
        },
        //删除管理________________________________________________________________
        async delFile(payload: any) {
            const data = await delFileApi(this.itemObj ? this.itemObj.id : '');
            if (data.statusCode === 200) {
                message.success('删除成功');
                this.getFileAction(payload);
            }
        },
        //删除管理
        // async delFile(payload: any) {
        //     const data = await delFileApi(this.itemObj ? this.itemObj.id : '');
        //     if (data.statusCode === 200) {
        //         message.success('删除成功');
        //         this.getFile(payload);
        //     }
        // },


    },
});

export default useStore;



