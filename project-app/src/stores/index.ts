import { defineStore } from 'pinia';
import * as user from '@/services/user';

const useStore = defineStore<string, any>('baseStore', {
  state: () => ({
    list: [],
    arr: [],
  }),
  actions: {
    async search(userForm: any) {
      const { data } = await user.getList(userForm);
      this.$patch({
        list: data[0],
      });
      return data[0];
    },
    async searchList(userForm: any) {
      const { data } = await user.searchList({ page: 1, pageSize: 12, ...userForm });
      const arr = data[0].filter(item => {
        return item.category;
      });
      const brr: any = [];
      arr.forEach((value, index) => {
        if (brr.indexOf(value.category.label) == -1) {
          brr.push(value.category);
        }
      });
      console.log(brr);
      
      this.$patch({
        arr: brr,
      });
    },
  },
});

export default useStore;
