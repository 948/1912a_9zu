import knowledge from './knowledge/index.vue';
import article from './article.vue';
import articleAll from './article/all.vue';
import articleCategory from './article/category.vue';
import articleTags from './article/tags.vue';
import user from './user/index.vue';
import search from "./search/index.vue";
import comment from "./comment/index.vue";
import file from "./file/index.vue";
import view from "./view/index.vue";
import mail from './mail/index.vue';
import page from "./page/index.vue";
import setting from './setting/index.vue';
// 所有视图的出口

export default {
  article,
  knowledge,
  articleAll,
  articleCategory,
  articleTags,
  user,
  search,
  comment,
  file,
  view,
  mail,
  page,
  setting,
};
