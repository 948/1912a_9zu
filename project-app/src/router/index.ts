import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';
// 创建路由实例^,创建一个历史记录^，创建一个 hash 历史记录^
import BasicLayout from '../layouts/BasicLayout.vue';
import authList from '../auth/user.js';
import notFoundVue from '../views/notFound.vue';
import viewConfig from '@/views/view.config';

const hasGithubPages = import.meta.env.VITE_GHPAGES;
// 基础路由 初始1
const baseRouter = [
  {
    path: '/worker',
    name: 'worker',
    meta: { title: '工作台', icon: 'icon-icon-test' },
    component: () => import('@/views/worker/WorkerPage.vue'), //工作台路径
  },
];
// 创建 一个路由实例 初始2
const router = createRouter({
  history: hasGithubPages ? createWebHashHistory() : createWebHistory(),
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: { title: '登录', showMenuItem: false },
      component: () => import('@/views/login/LoginView.vue'),
    },
    {
      path: '/register',
      name: 'register',
      meta: { title: '注册', showMenuItem: false },
      component: () => import('@/views/register/index.vue'),
    },
    {
      path: '/',
      name: 'index',
      meta: { title: 'Home', showMenuItem: false },
      component: BasicLayout,
      redirect: '/worker',
      children: [
        ...baseRouter,
        {
          path: '/404',
          name: '404',
          meta: { title: '页面找不到', icon: 'icon-icon-test', showMenuItem: false },
          component: () => import('@/views/notFound.vue'),
        },
        {

          path: "/search",
          name: "search",
          meta: { title: "搜索", showMenuItem: false },
          component: () => import("@/views/search/index.vue")
        },
        {
          path: "/comment",
          name: "comment",
          meta: { title: "评论", showMenuItem: false },
          component: () => import("@/views/comment/index.vue")
        },
        {
          path: "/file",
          name: "file",
          meta: { title: "文件管理", showMenuItem: false },
          component: () => import("@/views/file/index.vue")
        },
        {
          path: "/view",
          name: "view",
          meta: { title: "访问统计", showMenuItem: false },
          component: () => import("@/views/view/index.vue")
        },
        {
          path: '/search',
          name: 'search',
          meta: { title: '搜索', showMenuItem: false },
          component: () => import('@/views/search/index.vue'),
        },
        {
          path: '/comment',
          name: 'comment',
          meta: { title: '评论', showMenuItem: false },
          component: () => import('@/views/comment/index.vue'),
        },
        {
          path: '/file',
          name: 'file',
          meta: { title: '文件管理', showMenuItem: false },
          component: () => import('@/views/file/index.vue'),
        },
        {
          path: '/view',
          name: 'view',
          meta: { title: '访问统计', showMenuItem: false },
          component: () => import('@/views/view/index.vue'),
        },
        // {
        //   path: '/file',
        //   name: 'File',
        //   meat: { title: '文章管理', showMenuItem: false },
        //   component: () => import('@/views/file/index.vue'),
        // },
        {
          path: '/mail',
          name: 'Mail',
          meta: { title: '邮件管理', showMenuItem: false },
          component: () => import('@/views/mail/index.vue'),
        },
        {
          path: '/page',
          name: 'Page',
          meta: { title: '页面管理', showMenuItem: false },
          component: () => import('@/views/page/index.vue'),
        },
        {
          path:"/page",
          name:"page",
          meta:{title:"页面管理",showMenuItem: false},
          component:()=>import("@/views/page/index.vue")
        },
        {
          path:"/setting",
          name:"setting",
          meta:{title:"系统设置",showMenuItem: false},
          component:()=>import("@/views/setting/index.vue")
        },
      ],
    },
  ],
});
// 格式化name
const formatName = (val: string) =>
  val
    .split('/')
    .filter(v => v)
    .map((item, i) => (i === 0 ? item : item[0].toUpperCase() + item.slice(1)))
    .join('');
    
// 生成侧边菜单数据结构
const menuItemChildren = arr =>
  arr.map(({ path, title, children }) => {
    const name = formatName(path);
    const item = {
      path,
      name,
      meta: {
        title,
        icon: 'icon-icon-test',
      },
      component: viewConfig[name] ? viewConfig[name] : notFoundVue,
    };
    children && Array.isArray(children) && (item.children = menuItemChildren(children));
    return item;
  });
//设置白名单
const wihleList = ['/login', '/registry'];
// 获取用户身份 1 1111
export const getCurUserRole = () => {
  // 得到当前用户视图权限列表
  if (window.location.pathname === '/login') { //用户浏览器 页面地址是否在 登录页
    return [];
  }
  // 1. 取到 当前用户的 身份（role）
  const role = JSON.parse(window.localStorage.getItem('userInfo'))?.role;
  // 用户没有登录
  if (!role) {
    router.replace('/login');//当没有身份 回到登录页
    return [];
  }
  // 当前用户的权限
  const curUserRole = authList.find(val => val.role === role);//authList见 在
  // 当前用户的视图权限
  const viewsRole = menuItemChildren(curUserRole?.viewList);
  // console.log('viewsRole:---------', viewsRole);
  return viewsRole;
};

// 导航的数据
export const addRoutes = routes => { /// 33333333
  routes.forEach(item => {
    router.addRoute('index', item); // 添加到路由表
  });
};
// 获取 基础路由
export const getBaseRoutes = () => {
  return baseRouter;
};
// 添加一条新的路由作为 routes 的子路由
addRoutes(getCurUserRole()); // 添加路由视图  22222


export default router;
